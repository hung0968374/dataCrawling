const cheerio = require("cheerio");

const request = require("request-promise");
const fs = require("fs"); // require thêm module filesystem
var urls = [
  "https://phongvu.vn/may-tinh-de-ban-pc-asus-rog-strix-gl10cs-i5-94008gb512g-ssdrtx2060-6gbwin10-gl10csvn023t-s200301104.html",
  "https://phongvu.vn/may-tinh-de-ban-pc-asus-rog-huracan-g21cn-i5-9400f8gb256gb-ssdrtx2060-6gbwin10-g21cndvn001t-s200100137.html",
];

let data = [];
for (var url of urls) {
  request(url, (error, response, html) => {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html); // load HTML
      let imgs = [];
      let h2Title = [];
      let content_p = [];
      let imgsInReview = [];
      let processor = "";
      let screen = "";
      let ram = "";
      let graphicCard = "";
      let operatingSystem = "";
      let weight = "";
      let ssd = "";
      const genre = "pc";
      const title = $(".css-1jpdzyd").text();
      const price = $(".css-3725in").text();
      $(" .css-7j9rw7").each((index, el) => {
        if (index === 4) {
          processor = $(el).text();
        }
        if (index === 5) {
          ram = $(el).text();
        }
        if (index === 6) {
          ssd = $(el).text();
        }
        if (index === 7) {
          operatingSystem = $(el).text();
        }
        if (index === 8) {
          graphicCard = $(el).text();
        }
        if (index === 13) {
          weight = $(el).text();
        }
        if (index === 14) {
          screen = $(el).text();
        }
      });
      $(".css-4vw0bo").each((index, el) => {
        const img = $(el).find("img").attr("src");
        if (img) {
          imgs.push(img);
        }
      });
      $(".css-cqdbw1").each((index, el) => {
        const img = $(el).find("img").attr("src");
        if (img) {
          imgs.push(img);
        }
      });
      $(".css-111s35w h2").each((index, el) => {
        const h2text = $(el).text();
        h2Title.push(h2text);
      });
      $(".css-111s35w p").each((index, el) => {
        const ptext = $(el).text();
        content_p.push(ptext);
      });
      $(".css-111s35w figure img").each((index, el) => {
        const reviewImg = $(el).attr("src");
        imgsInReview.push(reviewImg);
      });
      const review = {
        title: h2Title,
        content: content_p,
        imgs: imgsInReview,
      };
      data.push({
        title,
        price,
        processor,
        ram,
        ssd,
        operatingSystem,
        graphicCard,
        weight,
        screen,
        imgs,
        review,
        genre,
      });
      fs.writeFileSync("data.json", JSON.stringify(data));
    } else {
      console.log(error);
    }
  });
}
