const cheerio = require("cheerio");

const request = require("request-promise");
const fs = require("fs"); // require thêm module filesystem
let data = [];
request("https://www.thegioididong.com/laptop", (error, response, html) => {
  if (!error && response.statusCode == 200) {
    const $ = cheerio.load(html); // load HTML

    $(".item").each((index, el) => {
      // lặp từng phần tử có class là job__list-item
      //   const price = $(el).find(".price strong").text(); // lấy tên job, được nằm trong thẻ a < .job__list-item-title
      const imgUrl =
        $(el).find("a").find("img").attr("data-original") ||
        $(el).find("a").find("img").attr("src"); // lấy tên job, được nằm trong thẻ a < .job__list-item-title
      data.push({
        imgUrl,
      });
    });
    fs.writeFileSync("data.json", JSON.stringify(data));
  } else {
    console.log(error);
  }
});
