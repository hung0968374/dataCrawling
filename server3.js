const cheerio = require("cheerio");

const request = require("request-promise");
const fs = require("fs"); // require thêm module filesystem
var urls = [
  "https://thinkpro.vn/lenovo-thinkpad-x1-nano?option=X1Nano02NS",
  "https://thinkpro.vn/hp-pavilion-15-intel-gen-11?option=Pavi15G1108CF",
  "https://thinkpro.vn/lenovo-thinkpad-p1-gen-3?option=ThinkPadP1G302NF",
  "https://thinkpro.vn/lenovo-thinkpad-x1-carbon-gen-8?option=ThinkPadX1CG806NF",
];
var promises = [];
let data = [];
var i;
for (var url of urls) {
  promises.push(
    new Promise((resolve, reject) => {
      request(url, function (err, respone, html) {
        if (err) {
          return reject(err);
        }
        var $ = cheerio.load(html);
        var items = [];
        $(".productdetail")
          .find(".slideshow__gallery-slide")
          .each(function (index, element) {
            const job = $(element).find("span").find("img").attr("src"); // lấy tên job, được nằm trong thẻ a < .job__list-item-title
            items.push(job);
          });
        return resolve(items);
      });
    })
  );
}
Promise.all(promises)
  .then((results) => {
    console.log(results);
    fs.writeFileSync("data.json", JSON.stringify(results));
  })
  .catch((error) => {
    console.log(error);
  });
