const cheerio = require("cheerio");

const request = require("request-promise");
const fs = require("fs"); // require thêm module filesystem
var url = [
  "https://thinkpro.vn/lenovo-thinkpad-x1-nano?option=X1Nano02NS",
  "https://thinkpro.vn/hp-pavilion-15-intel-gen-11?option=Pavi15G1108CF",
];
let data = [];
var i = 0;
request(url[0], (error, response, html) => {
  if (!error && response.statusCode == 200) {
    const $ = cheerio.load(html); // load HTML
    let imgs = [];
    $(".slideshow__gallery-slide").each((index, el) => {
      // lặp từng phần tử có class là job__list-item
      const job = $(el).find("span").find("img").attr("src"); // lấy tên job, được nằm trong thẻ a < .job__list-item-title
      imgs.push(job);
    });
    console.log("imgs", imgs);
    data.push({
      imgs,
    });
    fs.writeFileSync("data.json", JSON.stringify(data));
  } else {
    console.log(error);
  }
});
