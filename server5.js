const cheerio = require("cheerio");

const request = require("request-promise");
const fs = require("fs"); // require thêm module filesystem
let data = [];
request(
  "https://www.hanoicomputer.vn/pc-lenovo-legion-t530-28icb-i7-9700-16gb-ram-256gb-ssd-1tb-hdd-rtx2060s-8gb-wl-bt-k-m-no-os-90l300jtvn",
  (error, response, html) => {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html); // load HTML
      const title = $(
        ".product-detail-top .product_detail-header .product_detail-title"
      )
        .find("h1")
        .text();
      let imgs = [];
      $("#img_thumb .owl-thumb-item").each((index, el) => {
        const img = $(el).find("img").attr("src");
        imgs.push(img);
      });
      $(".nd .product_detail-title").children((child) => {
        const el = $(child);
        console.log(el);
      });
      data.push({
        title,
        imgs,
      });
      fs.writeFileSync("data.json", JSON.stringify(data));
    } else {
      console.log(error);
    }
  }
);
