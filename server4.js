const cheerio = require("cheerio");

const request = require("request-promise");
const fs = require("fs"); // require thêm module filesystem
var urls = [
  "https://thinkpro.vn/lenovo-thinkpad-t490?option=ThinkPadT49011NS",
  "https://thinkpro.vn/surface-laptop-2-135?option=SurfaceLaptop213501NU",
  "https://thinkpro.vn/dell-xps-13-9300?option=XPS930005NS",
  "https://thinkpro.vn/dell-alienware-m15-r2?option=AlienwareM15R210NU",
  "https://thinkpro.vn/apple-macbook-air-chinh-hang-apple-m1-late-2020?option=MacbookAir13L20H01CS",
  "https://thinkpro.vn/dell-xps-13-7390?option=XPS739001NU",
  "https://thinkpro.vn/hp-probook-450-g8-chinh-hang?option=Probook450G801CF",
  "https://thinkpro.vn/asus-zenbook-13-ux325?option=ZenbookUX32502CF",
  "https://thinkpro.vn/dell-inspiron-14-5402?option=Inspiron540201NS",
];
let data = [];
for (var url of urls) {
  request(url, (error, response, html) => {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(html);
      let imgs = [];
      let title = "";
      let processor = "";
      let screen = "";
      let ram = "";
      let graphicCard = "";
      let pin = "";
      let weight = "";
      let operatingSystem = "";
      let price = "";
      let review = [];
      $(".slideshow__gallery-slide").each((index, el) => {
        const img = $(el).find("span").find("img").attr("src");
        imgs.push(img);
      });
      $(".productdetail__info").each((index, el) => {
        title = $(el).find("h1").text();
        $(el)
          .find("ul")
          .find("li")
          .each((index, el) => {
            if (index == 5) {
              processor = $(el).text();
            } else if (index == 6) {
              screen = $(el).text();
            } else if (index == 8) {
              ram = $(el).text();
            } else if (index == 9) {
              graphicCard = $(el).text();
            } else if (index == 11) {
              pin = $(el).text();
            } else if (index == 13) {
              weight = $(el).text();
            } else if (index == 14) {
              operatingSystem = $(el).text();
            }
          });
      });
      $(".rte")
        .find("p")
        .each((index, el) => {
          if ($(el).text().length > 0) review.push($(el).text());
        });

      price = $(".sticky_detail").find(".price strong").text();
      data.push({
        imgs,
        title,
        processor,
        screen,
        ram,
        graphicCard,
        pin,
        weight,
        operatingSystem,
        price,
        review,
      });
      fs.writeFileSync("data.json", JSON.stringify(data));
    } else {
      console.log(error);
    }
  });
}
